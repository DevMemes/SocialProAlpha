<?php

/**
 * Created by PhpStorm.
 * User: marwen
 * Date: 19/02/2017
 * Time: 18:37
 */


namespace SocialPro\EntrepriseBundle\Menu;

    use Knp\Menu\FactoryInterface;
    use Symfony\Component\DependencyInjection\ContainerAwareInterface;
    use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');

        $menu->addChild('Mes publications', array('route' => 'social_pro_entreprise_mypubli'));
        $menu->addChild('mes messages', array('route' => 'fos_message_inbox'));
        // ... add more children

        return $menu;
    }

}