<?php
namespace SocialPro\EntrepriseBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * PubliEntreprise
 *
 * @ORM\Table(name="PubliEntreprise")
 * @ORM\Entity(repositoryClass="SocialPro\EntrepriseBundle\Repository\PubliEntrepriseRepository")
 */





class PubliEntreprise
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var int
     *
     * @ORM\Column(name="id_user", type="integer",nullable=true)
     */
    private $idUser;
    /**
     * @var int
     *
     * @ORM\Column(name="id_entreprise", type="integer",nullable=true)
     */
    private $idEntreprise;
    /**
     * @var string
     *
     * @ORM\Column(name="message",type="string",length=500,nullable=true)
     */
    private $message;
    /**
     * @ORM\Column(type="date")
     */
    private $time;
    /**
     * @var string
     *
     * @ORM\Column(name="url",type="string",length=500,nullable=true)
     */
    private $url;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }




    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdUser()
    {
        return $this->idUser;
    }

    /**
     * @param int $idUser
     */
    public function setIdUser($idUser)
    {
        $this->idUser = $idUser;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param int $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }


}