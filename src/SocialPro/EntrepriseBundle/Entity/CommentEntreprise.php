<?php

namespace SocialPro\EntrepriseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommentEntreprise
 *
 * @ORM\Table(name="comment_entreprise")
 * @ORM\Entity(repositoryClass="SocialPro\EntrepriseBundle\Repository\CommentEntrepriseRepository")
 */
class CommentEntreprise
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="publicationId", type="string", length=255)
     */
    private $publicationId;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set publicationId
     *
     * @param string $publicationId
     *
     * @return CommentEntreprise
     */
    public function setPublicationId($publicationId)
    {
        $this->publicationId = $publicationId;
    
        return $this;
    }

    /**
     * Get publicationId
     *
     * @return string
     */
    public function getPublicationId()
    {
        return $this->publicationId;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return CommentEntreprise
     */
    public function setMessage($message)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return CommentEntreprise
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}

