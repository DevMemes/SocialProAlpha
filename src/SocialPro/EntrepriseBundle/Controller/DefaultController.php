<?php

namespace SocialPro\EntrepriseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SocialProEntrepriseBundle:Default:index.html.twig');
    }
}
