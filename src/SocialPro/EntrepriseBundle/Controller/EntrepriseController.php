<?php

namespace SocialPro\EntrepriseBundle\Controller;

use SocialPro\EntrepriseBundle\Entity\CommentEntreprise;
use SocialPro\EntrepriseBundle\Entity\Entreprise;
use SocialPro\EntrepriseBundle\Entity\PubliEntreprise;
use SocialPro\EntrepriseBundle\Form\EntrepriseType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class EntrepriseController extends Controller
{
    public function ajouterAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $publientreprise = new PubliEntreprise();
        $form=$request->get('texts');
        $session= $this->getUser()->getId();
           $publientreprise->setIdUser($session);
            $session= $this->getUser()->getEntreprise();
            $publientreprise->setIdEntreprise($session);
            $publientreprise->setMessage($form);
            $publientreprise->setTime(new \DateTime('now'));


           $em->persist($publientreprise);
           $em->flush();

        return $this->redirectToRoute("social_pro_entreprise_espace");



    }

    public function modifierAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entreprise = $em->getRepository('SocialProEntrepriseBundle:Entreprise')->find($id);
        $form = $this->createForm(EntrepriseType::class,$entreprise);
        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->flush();
            return new Response('Entreprise modifiée avec succées');
        }
        $formView=$form->createView();
        return $this->render('SocialProEntrepriseBundle:Entreprise:Modifier.html.twig', array('form'=>$formView));
    }

    public function supprimerAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entreprise = $em->getRepository('SocialProEntrepriseBundle:Entreprise')->find($id);

        //if (null === $advert) {
        //    throw new NotFoundHttpException("L'annonce d'id " . $id . " n'existe pas.");
        //}

        // On crée un formulaire vide, qui ne contiendra que le champ CSRF
        // Cela permet de protéger la suppression d'annonce contre cette faille

        $em->remove($entreprise);
        $em->flush();

        return new Response('Entreprise supprimé avec succées');
    }

    public function afficherAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entreprise = $em->getRepository('SocialProEntrepriseBundle:Entreprise')->findBy(array('statut' => 0));

        return $this->render('SocialProEntrepriseBundle:Entreprise:afficher.html.twig', array('entreprise'=>$entreprise));
    }

    public function validerAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $qb = $em->createQueryBuilder();
        $qb->update('SocialProEntrepriseBundle:Entreprise', 'e')
            ->set('e.statut', 1)
            ->where('e.id = ' . $id)
            ->getQuery()
            ->execute();

        //$entreprise = $em->getRepository('SocialProEntrepriseBundle:Entreprise')->find($id);
        return new Response('Entreprise validée avec succées');
    }
    public function espaceAction()
    {

        $em = $this->getDoctrine()->getManager();
        $ident = $this->getUser()->getEntreprise();
        $publication =  $em->getRepository('SocialProEntrepriseBundle:PubliEntreprise')->findBy(array('idEntreprise' => $ident));
        $form = $this->container->get('fos_message.new_thread_form.factory')->create();
        $formHandler = $this->container->get('fos_message.new_thread_form.handler');

        $i = 0;

        foreach ($publication as $p)
        {

            $comment[$i] = $em->getRepository('SocialProEntrepriseBundle:CommentEntreprise')->findBy(array('publicationId' => $p));
            $i++;
        }
        if ($message = $formHandler->process($form)) {
            return new RedirectResponse($this->container->get('router')->generate('fos_message_thread_view', array(
                'threadId' => $message->getThread()->getId()
            )));
        }
        return $this->render('SocialProEntrepriseBundle:Entreprise:Espace.html.twig', array('data' => $form->getData(),'form' => $form->createView(),'publicaton'=>$publication,'comment'=>$comment));
    }
    public function mypubliAction()
    {
        $form = $this->container->get('fos_message.new_thread_form.factory')->create();
        $formHandler = $this->container->get('fos_message.new_thread_form.handler');
        $em = $this->getDoctrine()->getManager();
        $ident = $this->getUser()->getID();
        $publication =  $em->getRepository('SocialProEntrepriseBundle:PubliEntreprise')->findBy(array('idUser' => $ident));
        return $this->render('SocialProEntrepriseBundle:Entreprise:Mypubli.html.twig', array('data' => $form->getData(),'form' => $form->createView(),'publicaton'=>$publication));
    }
    public function testAction(){

        return $this->render('@FOSMessage/Message/threads_list.html.twig');
    }
    public function addcommentAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $commententreprise = new CommentEntreprise();
        $form=$request->get('texts');
        $forme=$request->get('texte');
        $commententreprise->setPublicationId($forme);
        $commententreprise->setMessage($form);
        $commententreprise->setDate(new \DateTime('now'));


        $em->persist($commententreprise);
        $em->flush();

        return $this->espaceAction();
    }
    public function editpubliAction(Request $request){
        $idd=$request->get('id');
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('SocialProEntrepriseBundle:PubliEntreprise')->findOneBy(array('id' =>$idd));

        $item->setMessage($request->get('text'));
        $em->flush();






        return $this->mypubliAction();
    }
    public function delAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $idd=$request->get('id');
        $publi = $em->getRepository('SocialProEntrepriseBundle:PubliEntreprise')->find($idd);


        $em->remove($publi);
        $em->flush();
        return $this->mypubliAction();
    }
}

