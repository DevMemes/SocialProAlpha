<?php
/**
 * Created by PhpStorm.
 * User: Guezz
 * Date: 10/02/2017
 * Time: 05:35
 */

namespace SocialPro\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SocialPro\UserBundle\Entity\User;

/**
 * TrackEmploi
 *
 * @ORM\Table(name="trackemploi")
 * @ORM\Entity(repositoryClass="SocialPro\MainBundle\Repository\TrackEmploiRepository")
 */
class TrackEmploi
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\MainBundle\Entity\EmploiFinal")
     */
    private $emploiFinal;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $message;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmploiFinal()
    {
        return $this->emploiFinal;
    }

    /**
     * @param mixed $emploiFinal
     */
    public function setEmploiFinal($emploiFinal)
    {
        $this->emploiFinal = $emploiFinal;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }






}