<?php

namespace SocialPro\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="status")
 */
class Status
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string",length=255)
     */
    protected $status;
    /**
     * @ORM\Column(type="integer")
     */
    protected $userid;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    protected $time;

    function __construct() {
        $this->userid = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    function getStatus() {
        return $this->status;
    }

    function getUserid() {
        return $this->userid;
    }

    function getTime() {
        return $this->time;
    }

    function setStatus($status) {
        $this->status = $status;
    }

    function setUserid($userid) {
        $this->userid = $userid;
    }

    function setTime($time) {
        $this->time = $time;
    }


}
