<?php
/**
 * Created by PhpStorm.
 * User: Espoir
 * Date: 09/02/2017
 * Time: 23:10
 */

namespace SocialPro\MainBundle\Entity;
use Doctrine\ORM\Mapping as espoir;

/**
 * @espoir\Entity
 */
class PostEmploi
{
    /**
     * @espoir\Id
     * @espoir\GeneratedValue
     * @espoir\Column(type="integer")
     */
    private  $id;

    /**
     * @espoir\Column(type="string",nullable=true)
     */
    private $email;

    /**
     * @espoir\Column(type="string")
     */
    private $status="En Cours";

    /**
     * @espoir\Column(type="datetime")
     */
    private $date;

    /**
     *@espoir\ManyToOne(targetEntity="Emploi")
     *@espoir\JoinColumn(name="id_emploi", referencedColumnName="id",onDelete="CASCADE",nullable=true)
     */
    private $id_emploi;


    /**
     *@espoir\ManyToOne(targetEntity="SocialPro\UserBundle\Entity\User")
     *@espoir\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE",nullable=true)
     */
    private $user;

    /**
     * PostEmploi constructor.
     * @param $date
     */
    public function __construct()
    {
        $this->date = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getIdEmploi()
    {
        return $this->id_emploi;
    }

    /**
     * @param mixed $id_emploi
     */
    public function setIdEmploi($id_emploi)
    {
        $this->id_emploi = $id_emploi;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }





}