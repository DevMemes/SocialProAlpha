<?php

namespace SocialPro\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SocialPro\UserBundle\Entity\User;

/**
 * VoteComm
 *
 * @ORM\Table(name="votecomm")
 * @ORM\Entity(repositoryClass="SocialPro\MainBundle\Repository\VoteCommRepository")
 */
class VoteComm
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\MainBundle\Entity\Comment")
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * @param mixed $commentaire
     */
    public function setCommentaire(Comment $commentaire)
    {
        $this->commentaire = $commentaire;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }


}