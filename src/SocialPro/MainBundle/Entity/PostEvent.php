<?php
/**
 * Created by PhpStorm.
 * User: Espoir
 * Date: 09/02/2017
 * Time: 22:40
 */

namespace SocialPro\MainBundle\Entity;
use Doctrine\ORM\Mapping as espoir;

/**
 * @espoir\Entity
 */
class PostEvent
{
    /**
     * @espoir\Id
     * @espoir\GeneratedValue
     * @espoir\Column(type="integer")
     */
   private  $id;

    /**
     * @espoir\Column(type="string")
     */
    private $status="En Cours";

    /**
     * @espoir\Column(type="string")
     */
    private $email="myAd@yahoo.fr";



    /**
     * @espoir\Column(type="integer",nullable=true)
     */
    private $tel=0;


    /**
     * @espoir\Column(type="integer")
     */
    private $nombrePlace=1;

    /**
     *@espoir\ManyToOne(targetEntity="SocialPro\UserBundle\Entity\User")
     *@espoir\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE",nullable=true)
     */
    private $user;

    /**
     *@espoir\ManyToOne(targetEntity="Evenement")
     *@espoir\JoinColumn(name="id_event", referencedColumnName="id",onDelete="CASCADE",nullable=true)
     */
    private $id_event;


    /**
     * @espoir\Column(type="datetime")
     */
    private $date;

    /**
     * PostEvent constructor.
     * @param $date
     */
    public function __construct()
    {
        $this->date = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param mixed $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return mixed
     */
    public function getNombrePlace()
    {
        return $this->nombrePlace;
    }

    /**
     * @param mixed $nombrePlace
     */
    public function setNombrePlace($nombrePlace)
    {
        $this->nombrePlace = $nombrePlace;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getIdEvent()
    {
        return $this->id_event;
    }

    /**
     * @param mixed $id_event
     */
    public function setIdEvent($id_event)
    {
        $this->id_event = $id_event;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }




}