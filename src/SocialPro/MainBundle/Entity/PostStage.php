<?php
/**
 * Created by PhpStorm.
 * User: Espoir
 * Date: 09/02/2017
 * Time: 23:25
 */

namespace SocialPro\MainBundle\Entity;
use Doctrine\ORM\Mapping as espoir;

/**
 * @espoir\Entity
 */
class PostStage
{

    /**
     * @espoir\Id
     * @espoir\GeneratedValue
     * @espoir\Column(type="integer")
     */
    private  $id;

    /**
     * @espoir\Column(type="string")
     */
    private $status="En Cours";

    /**
     * @espoir\ManyToOne(targetEntity="SocialPro\UserBundle\Entity\User",inversedBy="postStage")
     * @espoir\JoinColumn(name="user_id", referencedColumnName="id",nullable=true)
     */
    protected $user;

    /**
     * @espoir\ManyToOne(targetEntity="SocialPro\MainBundle\Entity\Stage",inversedBy="stage_post")
     * @espoir\JoinColumn(name="id_Stage", referencedColumnName="id",nullable=true)
     */
    protected $id_Stage;


    /**
     * @espoir\Column(type="datetime")
     */
    private $date;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getIdStage()
    {
        return $this->id_Stage;
    }

    /**
     * @param mixed $id_Stage
     */
    public function setIdStage($id_Stage)
    {
        $this->id_Stage = $id_Stage;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


}