<?php
/**
 * Created by PhpStorm.
 * User: Espoir
 * Date: 01/02/2017
 * Time: 15:18
 */

namespace SocialPro\MainBundle\Entity;
use Doctrine\ORM\Mapping as espoir;

/**
 * @espoir\Entity
 */
class Emploi
{
    /**
     * @espoir\Column(type="integer")
     * @espoir\Id
     * @espoir\GeneratedValue
     */
    private $id;




    /**
     * @espoir\Column(type="string",nullable=true)
     *
     */
    private  $nom;

    /**
     * @espoir\Column(type="string",nullable=true)
     *
     */
    private  $email;

    /**
     * @espoir\Column(type="integer",nullable=true)
     *
     */
    private  $tel;

    /**
     * @espoir\Column(type="string",nullable=true)
     *
     */
    private  $langues;


    /**
     * @espoir\Column(type="string",nullable=true)
     *
     */
    private  $ville;

    /**
     * @espoir\Column(type="string",nullable=true)
     *
     */
    private  $codePostal;



    /**
     * @espoir\Column(type="text",length=3000,nullable=true)
     *
     */
    private  $description;


    /**
     * @espoir\Column(type="text",length=1500,nullable=true)
     *
     */
    private  $competance;




    /**
     * @espoir\Column(type="text",length=1500,nullable=true)
     *
     */
    private  $experience;


    /**
     *  @espoir\Column(type="datetime")
     */
    private $date;



    /**
     *@espoir\ManyToOne(targetEntity="SocialPro\UserBundle\Entity\User")
     *@espoir\JoinColumn(name="user_id", referencedColumnName="id",onDelete="CASCADE",nullable=true)
     */
    private $user;



    /**
     * Emploi constructor.
     * @param $date
     */
    public function __construct()
    {
        $this->date = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param mixed $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return mixed
     */
    public function getLangues()
    {
        return $this->langues;
    }

    /**
     * @param mixed $langues
     */
    public function setLangues($langues)
    {
        $this->langues = $langues;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return mixed
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * @param mixed $codePostal
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;
    }

    /**
     * @return mixed
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * @param mixed $experience
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCompetance()
    {
        return $this->competance;
    }

    /**
     * @param mixed $competance
     */
    public function setCompetance($competance)
    {
        $this->competance = $competance;
    }




}