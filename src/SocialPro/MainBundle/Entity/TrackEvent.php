<?php
/**
 * Created by PhpStorm.
 * User: Guezz
 * Date: 10/02/2017
 * Time: 05:57
 */

namespace SocialPro\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SocialPro\UserBundle\Entity\User;

/**
 * TrackEvent
 *
 * @ORM\Table(name="trackevent")
 * @ORM\Entity(repositoryClass="SocialPro\MainBundle\Repository\TrackEventRepository")
 */
class TrackEvent
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\MainBundle\Entity\EvenementFinal")
     */
    private $eventFinal;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEventFinal()
    {
        return $this->eventFinal;
    }

    /**
     * @param mixed $eventFinal
     */
    public function setEventFinal($eventFinal)
    {
        $this->eventFinal = $eventFinal;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }




}