<?php
/**
 * Created by PhpStorm.
 * User: Espoir
 * Date: 10/02/2017
 * Time: 20:13
 */

namespace SocialPro\MainBundle;
use Symfony\Component\HttpFoundation\File\UploadedFile;


class FileUploader
{
    private $targetDir;

    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->targetDir, $fileName);

        return $fileName;
    }
}