<?php

namespace SocialPro\MainBundle\Controller;
use SocialPro\MainBundle\Entity\Friends;
//use SocialPro\MainBundle\Form\MailType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use SocialPro\UserBundle\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class SocialProController extends Controller
{
    public function indexAction()
    {
        return $this->render('SocialProMainBundle:SocialPro:index.html.twig');
    }

    public function mainpagepersonneAction()
    {
        return $this->render('SocialProMainBundle:Roles:personne.html.twig');
    }

    public function mainpageentrepriseAction()
    {
        return $this->render('SocialProMainBundle:Roles:entreprise.html.twig');
    }
    public function mainpageAdminAction()
    {
        return $this->render('SocialProMainBundle:Roles:admin.html.twig');
    }

    public function profileAction()
    {
        return $this->render('SocialProMainBundle:SocialPro:profile.html.twig');
    }





    public function networkfriendsAction()
    {
        return $this->render('SocialProMain:SocialPro:networkfriends');
    }





    public function parametresprofilAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $fosuser= $this->getUser();
        if(!$fosuser){
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }


        $id = $fosuser->getId();
        $user= $em->getRepository("SocialProUserBundle:User")->findOneBy(array('id'=> $id));
        //$user = new User();
        //$user->setEmail("wathmal@gmail.com");
        //$user->setPlainpassword("abc");
        //$user->setPicture("");

        //if no user in testBundle/User entity table create it.


        $form = $this->createFormBuilder($user)
            ->add('nom', TextType::class)
            ->add('sexe', ChoiceType::class, array('label'=>'Sexe',
                'choices'   => array('femme' => 'Femme',
                    'homme' => 'Homme'),
                'multiple'=>false,
                'required' => true
            ))
            ->add('pays', CountryType::class)

            ->add('datedenaissance', DateType::class, array(
                // render as a single text box
                'widget' => 'single_text',
            ))

            ->add('skills', TextareaType::class)
            /* ->add('skills', ChoiceType::class, array('label'=>'Vous maitrisez quoi?',
                 'choices'   => array('Java' => 'Java',
                     'C#' => 'C#',
                     'Python' => 'Python',
                     'Php' => 'Php',
                     'Android' => 'Android',
                     'CodeName One' => 'Codename one',
                     'Oracle' => 'Oracle'),
                 'multiple'=> true,
                 'required' => true
             ))*/

            ->add('job', TextType::class)
            ->add('addressjob', TextType::class)
            ->add('description', TextareaType::class)

            ->add('addressemailperso', EmailType::class)



            ->add('save', SubmitType::class, array('label' => 'save changes'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {

            $user->setNom($form->get('nom')->getData());
            $user->setSexe($form->get('sexe')->getData());
            $user->setPays($form->get('pays')->getData());

            $user->setDatedenaissance($form->get('datedenaissance')->getData());

            $user->setSkills($form->get('skills')->getData());
            $user->setAddressemailperso($form->get('addressemailperso')->getData());
            $user->setJob($form->get('job')->getData());
            $user->setAddressjob($form->get('addressjob')->getData());


            $user->setDescription($form->get('description')->getData());

            //$em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('parametresprofil'));
        }

        return $this->render('SocialProMainBundle:SocialPro:parametresprofil.html.twig', array('form' => $form->createView(),'user'=>$fosuser));
    }


    public function entrepriseAction()
    {
        return $this->render('SocialProMainBundle:SocialPro:entrepriseprofil.html.twig');
    }



    public function parametreentrepriseAction(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $fosuser= $this->getUser();
        if(!$fosuser){
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }


        $id = $fosuser->getId();
        $user= $em->getRepository("SocialProUserBundle:User")->findOneBy(array('id'=> $id));
        //$user = new User();
        //$user->setEmail("wathmal@gmail.com");
        //$user->setPlainpassword("abc");
        //$user->setPicture("");

        //if no user in testBundle/User entity table create it.


        $form = $this->createFormBuilder($user)
            ->add('nom', TextType::class)
            ->add('natureentreprise', ChoiceType::class, array('label'=>'Nature',
                'choices'   => array('Publique' => 'Publique',
                    'Privé' => 'Privé',
                 'Semi-publique' => 'Semi-publique'),
                'multiple'=>false,
                'required' => true
            ))
            ->add('pays', CountryType::class)
            ->add('telentreprise', NumberType::class)

            ->add('datedecreation', DateType::class, array(
                // render as a single text box
                'widget' => 'single_text',
            ))

            ->add('domaine', TextareaType::class)
            /* ->add('skills', ChoiceType::class, array('label'=>'Vous maitrisez quoi?',
                 'choices'   => array('Java' => 'Java',
                     'C#' => 'C#',
                     'Python' => 'Python',
                     'Php' => 'Php',
                     'Android' => 'Android',
                     'CodeName One' => 'Codename one',
                     'Oracle' => 'Oracle'),
                 'multiple'=> true,
                 'required' => true
             ))*/


            ->add('description', TextareaType::class)

            ->add('addressemailperso', EmailType::class)



            ->add('save', SubmitType::class, array('label' => 'save changes'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isValid()) {

            $user->setNom($form->get('nom')->getData());
            $user->setNatureentreprise($form->get('nature')->getData());
            $user->setPays($form->get('pays')->getData());
            $user->setDatedecreation($form->get('datedecreation')->getData());
            $user->setDomaine($form->get('domaine')->getData());
            $user->setAddressemailperso($form->get('addressmailperso')->getData());
            $user->setTelentreprise($form->get('addressmailperso')->getData());

            $user->setDescription($form->get('description')->getData());

            //$em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('parametresentreprise'));
        }

        return $this->render('SocialProMainBundle:SocialPro:parametresprofilentreprise.html.twig', array('form' => $form->createView(),'user'=>$fosuser));
    }








    public function entreprisevisitAction($id)
    {
        return $this->render('SocialProMainBundle:SocialPro:entreprisevisited.html.twig');
    }

    public function profilevisitAction($id)
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('SocialProUserBundle:User');

        $project = $repository->find($id);
        return $this->render('SocialProMainBundle:SocialPro:profilevisited.html.twig',array('fayrouz'=>$project));
    }

    public function connectAction($id)
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('SocialProUserBundle:User');

        $project = $repository->find($id);
        $session = $this->getUser();
        $friend = new Friends();
        $friend->setUser($session);
        $friend->setUser2($project);
        $em = $this->getDoctrine()->getManager();
        $em->persist($friend);
        $em->flush();

        return $this->render('SocialProMainBundle:SocialPro:profilevisited.html.twig');
    }

    /*public function contactAction(Request $request)
    {
        // Create the form according to the FormType created previously.
        // And give the proper parameters
        $form = $this->createForm('SocialPro\MainBundle\Form\ContactType',null,array(
            // To set the action use $this->generateUrl('route_identifier')
            'action' => $this->generateUrl('contactmail'),
            'method' => 'POST'
        ));

        if ($request->isMethod('POST')) {
            // Refill the fields in case the form is not valid.
            $form->handleRequest($request);

            if($form->isValid()){
                // Send mail
                if($this->sendEmail($form->getData())){

                    // Everything OK, redirect to wherever you want ! :

                    return $this->redirectToRoute('parametresprofil');
                }else{
                    // An error ocurred, handle
                    var_dump("Errooooor :(");
                }
            }
        }

        return $this->render('SocialProMainBundle:SocialPro:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }

    private function sendEmail($data){
        $myappContactMail = 'devmemes2017@gmail.com';
        $myappContactPassword = 'devmemes@esprit';


        $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465,'ssl')
            ->setUsername($myappContactMail)
            ->setPassword($myappContactPassword);

        $mailer = \Swift_Mailer::newInstance($transport);

        $message = \Swift_Message::newInstance("Our Code World Contact Form ". $data["subject"])
            ->setFrom(array($myappContactMail => "Message by ".$data["name"]))
            ->setTo(array(
                $myappContactMail => $myappContactMail
            ))
            ->setBody($data["message"]."<br>ContactMail :".$data["email"]);

        return $mailer->send($message);
    }*/




   /* public function listAction()
    {

        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Friends')->findAll();
        return $this->render('SocialProMainBundle:SocialPro:profile.html.twig',array('m'=>$modele));

    }*/


    public function listadminusersAction()
    {

        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProUserBundle:User')->findAll();
        return $this->render('SocialProMainBundle:Admin:gestionutilisateuradmin.html.twig',array('m'=>$modele));

    }

    public function deleteadminusersAction($id)
    {


        $em=$this->getDoctrine()->getManager();
        $a=$em->getRepository('SocialProUserBundle:User')->find($id);

        $em->remove($id);
        $em->flush();


        return $this->redirectToRoute('gestionusers');    }
























}
