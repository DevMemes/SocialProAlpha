<?php

namespace SocialPro\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use SocialPro\UserBundle\Entity\User;

class ProfileController extends Controller
{

    public function searchAction(Request $request)
    {
        $em=$this->getDoctrine()->getManager();
        $data=$request->get('rechercher');
        $query=$em->createQuery(
            'SELECT p FROM SocialProUserBundle:User p
    WHERE (p.nom LIKE :data) OR (p.username LIKE :data)')
            ->setParameter('data', '%'.$data.'%');

        /*createQueryBuilder('f')
            ->where('f.nom = :nom')
            ->setParameter('nom',$data)
            ->orderBy('f.nom','ASC')
            ->getQuery();
        ->createQuery("
	            SELECT u FROM SocialProUserBundle:User u
	            WHERE u.nom LIKE :key "
    );*/
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('p', 1)/*page number*/,
            8/*limit per page*/
        );
        $pagination->setUsedRoute('search');

        return $this->render('@SocialProMain/Search/Search.html.twig', array(
            'pagination' => $pagination));

    }

}
