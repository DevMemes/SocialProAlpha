<?php

namespace SocialPro\MainBundle\Controller;

use SocialPro\MainBundle\Entity\Stage;
use SocialPro\MainBundle\Form\rechercherStage;
use SocialPro\MainBundle\Form\StageType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class StageController extends Controller
{
    public function indexStageAction(Request $requette)
    {
        $modele=new Stage();
        $em=$this->getDoctrine()->getEntityManager();
        $form=$this->createForm(rechercherStage::class);

        $form->handleRequest($requette);
        if($form->isValid()){
            $modele=$em->getRepository('SocialProMainBundle:Stage')->findBy(array('nom'=>$modele->getNom()));
        }
        /*else{
            $modele=$em->getRepository('SocialProMainBundle:Stage')->findAll();
        }  */
        return $this->render('SocialProMainBundle:Stage:indexStage.html.twig',array('form'=>$form->createView(),'modele'=>$modele));
    }
    public function  JoindreEvenementAction( $id){



        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em=$this->getDoctrine()->getManager();
        $modele2 = $em->getRepository("SocialProUserBundle:User")->find($user);

        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository("SocialProMainBundle:Evenement")->find($id);

        $modele2->setEvenements($modele);
        $em=$this->getDoctrine()->getManager();
        $em->persist($modele2);
        $em->flush();
        return $this->redirectToRoute('afficher_Evenement');

    }

    public function ajouterStageAction(Request $ma_requette)
    {
        $modele = new Stage();
        $form = $this->createForm(StageType::class, $modele);
        $form->handleRequest($ma_requette);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();

            return $this->redirectToRoute('afficher_Stage');
        }
        return $this->render('SocialProMainBundle:Stage:ajouterStage.html.twig', array('form' => $form->createView()));
    }

    public  function afficherStageAction(){
        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Stage')->findAll();
        return $this->render('SocialProMainBundle:Stage:afficherStage.html.twig',array('modele'=>$modele));
    }

    public function  supprimerStageAction($id){
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:Stage')->find($id);

        $em->remove($modele);
        $em->flush();

        return $this->redirectToRoute('afficher_Stage');
    }


}
