<?php

namespace SocialPro\MainBundle\Controller;

use SocialPro\MainBundle\Entity\Notification;
use SocialPro\MainBundle\Entity\Status;
use SocialPro\MainBundle\Entity\Vote;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use SocialPro\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;



class PostsController extends Controller
{
    public function showStatusAction(Request $request){
        $fosuser= $this->getUser();
        if(!$fosuser){
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        }

        $status = $this->getDoctrine()->getRepository('SocialProMainBundle:Status')->findAll();
        $users = $this->getDoctrine()->getRepository('SocialProUserBundle:User')->findAll();


        $newstatus= new Status();
        $em = $this->getDoctrine()->getManager();
        $originuser= $em->getRepository("SocialProUserBundle:User")->findOneBy(array('id'=> $this->getUser()->getId()));
        /*$form = $this->createFormBuilder($newstatus)
            ->add('status', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'post status'))
            ->getForm();
*/



        $arr= array();

        // retrieve statuses from latest to oldest :)
        // loop array backwards
        for($x=count($status)-1; $x>= 0; $x--) {
            $userid= $status[$x]->getUserid();
            $postid=$status[$x]->getId();
            $ln=$this->getDoctrine()->getManager();
            $userwlid = $this->get('security.token_storage')->getToken()->getUser();
            $votes=$ln->getRepository('SocialProMainBundle:Vote')->findBy(array('statusid'=>$postid,'user'=>$userwlid));
            $bool=0;
            if(count($votes)!=0)
            {
                $bool=1;
            }
            $username="";
            $picture="";
            foreach ($users as $user){
                if($user->getId() == $userid){
                    $username= $user->getNom();
                    $picture= $user->getAvatar(); /* $user->getPicture();*/
                }
            }
            $arr[$x]=array('username'=>$username, 'picture'=>$picture, 'id'=>$postid, 'voted'=>$bool, 'status'=> $status[$x]->getStatus());

        }
        if ($request->isMethod('POST')) {
            $pub=$request->get('publication');
            if (strlen($pub)==0){
                return $this->render('@SocialProMain/Roles/personne.html.twig', array('length' => count($status),'array'=> $arr));
            }

            $newstatus->setStatus($request->get('publication'));
            $newstatus->setTime(new \DateTime());
            $newstatus->setUserid($originuser->getId());

            $em->persist($newstatus);
            $em->flush();

            return $this->redirect($this->generateUrl('mainpage'));
        }


        return $this->render('@SocialProMain/Roles/personne.html.twig', array('length' => count($status),'array'=> $arr));

    }

    public function commentAction($id)
    {
        $em=$this->getDoctrine()->getManager();
        $post=$em->getRepository("SocialProMainBundle:Status")->find($id);
        $user=$em->getRepository("SocialProUserBundle:User")->find($post->getUserid());
        $username= $user->getNom();
        $picture= $user->getAvatar();

        return $this->render('@SocialProMain/Post/Post.html.twig', array('username'=>$username, 'picture'=>$picture, 'id'=>$id, 'status'=> $post->getStatus()));
    }

    public function voteAction($id)
    {
            $em=$this->getDoctrine()->getManager();
            $post=$em->getRepository("SocialProMainBundle:Status")->find($id);

            $vote=new Vote();
            $vote->setStatusid($post);
            $userwlid = $this->get('security.token_storage')->getToken()->getUser();

             $vote->setUser($userwlid);
            $em->persist($vote);
            $em->flush();
            /* NOTIFICATION */
            $notification=new Notification();
            $ntfc=$this->getDoctrine()->getManager();

            $user=$ntfc->getRepository("SocialProUserBundle:User")->find($userwlid);
            $post=$ntfc->getRepository("SocialProMainBundle:Status")->find($id);
            $likeduser=$ntfc->getRepository("SocialProUserBundle:User")->find($post->getUserid());

            $notification->setIdUser($likeduser);
            $notification->setSujet($user->getNom().'a voter pour votre publication: '.$post->getStatus());
            $notification->setLu(false);
            $ntfc->persist($notification);
            $ntfc->flush();

            return $this->redirectToRoute('mainpage');

        }

    public function AdminPostsAction()
    {
        return $this->render('@SocialProMain/Admin/GestPublications.html.twig');
    }
}
