<?php

namespace SocialPro\MainBundle\Controller;

use SocialPro\MainBundle\Entity\Emploi;
use SocialPro\MainBundle\Entity\PostEmploi;
use SocialPro\MainBundle\Form\EmploiType;
use SocialPro\MainBundle\Form\PostEmploiType;
use SocialPro\MainBundle\Form\rechecherPostEmploi;
use SocialPro\MainBundle\Form\rechercherEmploi;
use SocialPro\MainBundle\Form\rechercherEmploiType;
use SocialPro\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class EmploiController extends Controller
{
    public function indexEmploiAction(Request $requette)
    {
        $modele=new Emploi();
        $em=$this->getDoctrine()->getEntityManager();
        $form=$this->createForm(rechercherEmploiType::class,$modele);
        $form->handleRequest($requette);
        if($form->isValid()){
            $modele=$em->getRepository('SocialProMainBundle:Emploi')->findBy(array('nom'=>$modele->getNom()));
        }
        return $this->render('SocialProMainBundle:Emploi:indexEmploi.html.twig',array('form'=>$form->createView(),'modele'=>$modele));
    }



    public function ajouterEmploiAction(Request $ma_requette)
    {
        $modele = new Emploi();
        $form = $this->createForm(EmploiType::class, $modele);
        $form->handleRequest($ma_requette);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();

            return $this->redirectToRoute('afficher_Emploi');
        }elseif ($form->isValid()){

        }
        return $this->render('SocialProMainBundle:Emploi:ajouterEmploi.html.twig', array('form' => $form->createView()));
    }

    public function modifierEmploiAction(Request $ma_requette)
    {
        $modele = new Emploi();
        $form = $this->createForm(EmploiType::class, $modele);
        $form->handleRequest($ma_requette);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();

            return $this->redirectToRoute('afficher_Emploi');
        }
        return $this->render('SocialProMainBundle:Emploi:modifierEmploi.html.twig', array('form' => $form->createView()));
    }
    public  function  postulerEmploi2Action(Request $requette,$id){
        $eme=$this->getDoctrine()->getEntityManager();
        $modele2=$eme->getRepository('SocialProMainBundle:Emploi')->findAll();





        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Emploi')->find($id);

        $form=$this->createForm(PostEmploiType::class,$modele);
        $form->handleRequest($requette);
        if($form->isValid()){

            $postEmploi = new PostEmploi();
            $id_emp=$modele->getEmploiPost();
            $postEmploi->setIdEmploi($id_emp);

            $id_user=$modele->getUser();
            $postEmploi->setUser($id_user);

            $postEmploi->setStatus("En cours");
            $postEmploi->setDate(new \DateTime('now'));
            $em->persist($postEmploi);
            $em->flush();
            return $this->redirectToRoute('afficher_Emploi');
        }
        return $this->render('SocialProMainBundle:Emploi:afficherEmploi2.html.twig',array('modele'=>$modele2,'form_post_emploi'=>$form->createView()));
    }


    public  function afficherEmploiAction(){
        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Emploi')->findAll();
        return $this->render('SocialProMainBundle:Emploi:afficherEmploi.html.twig',array('modele'=>$modele));
    }

    public  function afficherEmploiEntrepriseAction(){
        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Emploi')->findAll();
        return $this->render('SocialProMainBundle:Emploi:afficherEmploiEntreprise.html.twig',array('modele'=>$modele));
    }

   public function VisiterOffreEmploiAction($id){
       $em=$this->getDoctrine()->getManager();
       $modele=$em->getRepository('SocialProMainBundle:Emploi')->find($id);
       //$mod=new Emploi();
      // $id=$mod->getId();
      // return $this->redirectToRoute('afficher_Emploi_VisiterOffreEmploi');

       //return $this->redirectToRoute('afficher_Emploi_VisiterOffreEmploi', array('id' => $mod->getId()));
       return $this->render('SocialProMainBundle:Emploi:visiterOffre.html.twig',array('modele'=>$modele));
   }




    public function  supprimerEmploiAction($id){
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:Emploi')->find($id);

        $em->remove($modele);
        $em->flush();

        return $this->redirectToRoute('afficher_Emploi');
    }
//////////////////////////////////////////////////////////////////  Zone Pour PostEmploi    ///////////////////////////////////////////////////////////


    public  function postulerEmploiAction($id){


        $username=$this->get('security.token_storage')->getToken()->getUser();
        $userManager = $this->container->get('fos_user.user_manager');
        //$username="blabla";




        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Emploi')->find($id);

        //
        $postEmploi = new PostEmploi();
        $modele->getId();
        // $id_emploi=$modele->getId();
        // $postEmploi->setIdEmploi($id_emp);
        $postEmploi->setIdEmploi($modele);

        $id_user=$modele->getUser();
        $postEmploi->setUser($id_user);

        $postEmploi->setStatus("En cours");
        $postEmploi->setEmail($username);
        $postEmploi->setDate(new \DateTime('now'));
        $em->persist($postEmploi);
        $em->flush();





        return $this->redirectToRoute('afficher_Emploi');
    }


    public  function postulerEmploiPersonneAction($id){


        $username=$this->get('security.token_storage')->getToken()->getUser();
        $userManager = $this->container->get('fos_user.user_manager');
        //$username="blabla";




        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Emploi')->find($id);

        //
        $postEmploi = new PostEmploi();
        $modele->getId();
        // $id_emploi=$modele->getId();
        // $postEmploi->setIdEmploi($id_emp);
        $postEmploi->setIdEmploi($modele);

        $id_user=$modele->getUser();
        $postEmploi->setUser($id_user);

        $postEmploi->setStatus("En cours");
        $postEmploi->setEmail($username);
        $postEmploi->setDate(new \DateTime('now'));
        $em->persist($postEmploi);
        $em->flush();





        return $this->redirectToRoute('afficher_Emploi_personne');
    }

    public function AfficherPostEmploiAction(){
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:PostEmploi')->findAll();

        return $this->render('SocialProMainBundle:PostEmploi:afficherPostEmploi.html.twig',array('modele'=>$modele));


    }




    public function indexPostEmploiAction(Request $requette)
    {
        $modele=new PostEmploi();
        $em=$this->getDoctrine()->getEntityManager();
        $form=$this->createForm(rechecherPostEmploi::class,$modele);
        $form->handleRequest($requette);
        if($form->isValid()){
            $modele=$em->getRepository('SocialProMainBundle:PostEmploi')->findBy(array('nom'=>$modele->getUser()));
        }
        return $this->render('SocialProMainBundle:Emploi:indexEmploi.html.twig',array('form'=>$form->createView(),'modele'=>$modele));
    }

    public function modifierPostEmploiAction(Request $ma_requette)
    {
        $modele = new PostEmploi();
        $form = $this->createForm(PostEmploiType::class, $modele);
        $form->handleRequest($ma_requette);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();

            return $this->redirectToRoute('afficher_Emploi');
        }
        return $this->render('SocialProMainBundle:Emploi:modifierEmploi.html.twig', array('form' => $form->createView()));
    }

    public function  RefuserEmploiAction($id){
        $em=$this->getDoctrine()->getManager();
        $etat=$em->getRepository('SocialProMainBundle:PostEmploi')->find($id);
        $modeles=$em->getRepository('SocialProMainBundle:PostEmploi')->findAll();
        $etat->setStatus('Refuser');
        $em->persist($etat);

        $em->flush();

        return  $this->redirect($this->generateUrl('afficher_PostEmploi'));

    }

    public function AccepterEmploiAction($id){
        $em=$this->getDoctrine()->getManager();
        $etat=$em->getRepository('SocialProMainBundle:PostEmploi')->find($id);
        $modeles=$em->getRepository('SocialProMainBundle:PostEmploi')->findAll();
        $etat->setStatus('Accepter');
        $em->persist($etat);

        $em->flush();

        return $this->redirect($this->generateUrl('afficher_PostEmploi'));
    }
    public  function rechercherPostEmploiAction(Request $requette){
        $modele=new PostEmploi();
        $em=$this->getDoctrine()->getEntityManager();
        $form=$this->createForm(rechecherPostEmploi::class,$modele);

        $form->handleRequest($requette);
        if($form->isValid()){
            $modele=$em->getRepository('SocialProMainBundle:PostEmploi')->findBy(array('email'=>$modele->getEmail()));
        }

        return $this->render('SocialProMainBundle:PostEvent:rechercherPostEvent.html.twig',array('form'=>$form->createView(),'modele'=>$modele));
    }


}
