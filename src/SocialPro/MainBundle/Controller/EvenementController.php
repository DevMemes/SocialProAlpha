<?php

namespace SocialPro\MainBundle\Controller;


use SocialPro\MainBundle\Entity\PostEvent;
use SocialPro\MainBundle\Form\PostEventType;
use SocialPro\MainBundle\Form\rechercherEmploiType;
use SocialPro\MainBundle\Form\rechercherPostEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use SocialPro\MainBundle\Entity\Evenement;
use SocialPro\MainBundle\Form\EvenementType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File;

class EvenementController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    public function ajouterEvenementAction(Request $la_requetteEspoir){

        $modele=new  Evenement();

        // $user = $this->get('security.context')->getToken()->getUser();

        $form = $this->createForm(EvenementType::class,$modele);
        $form->handleRequest($la_requetteEspoir);
        if($form->isValid()){


            // $model->setUser($user);
            // $file stores the uploaded PDF file
            /** @var (Symfony\Component\HttpFoundation\File\UploadedFile $file) */
            //   $file=$modele->getImage();
            //   $fileName = $this->get('app.image_uploader')->upload($file);

            // Update the 'brochure' property to store the PDF file name
            // instead of its contents

            //   $modele->setImage($fileName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();
            return $this->redirectToRoute('afficher_Evenement_ent');
        }
        return $this->render('SocialProMainBundle:Evenement:ajoutEvenement.html.twig',array('form'=>$form->createView()));


    }
    public function  JoindreEvenementAction( $id){



        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em=$this->getDoctrine()->getManager();
        $modele2 = $em->getRepository("SocialProUserBundle:User")->find($user);

        $em = $this->getDoctrine()->getManager();
        $modele = $em->getRepository("SocialProMainBundle:Evenement")->find($id);

        $modele2->setEvenements($modele);
        $em=$this->getDoctrine()->getManager();
        $em->persist($modele2);
        $em->flush();
        return $this->redirectToRoute('afficher_Evenement');

    }
    public function MapAction(){
        return $this->render('@SocialProMain/Evenement/try.html.twig');
    }
    public function Map1Action($id){

        $modele=new Evenement();
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:Evenement')->find($id);
        return $this->render('SocialProMainBundle:Evenement:placeDetails.html.twig',array('modele'=>$modele));
    }


    public function Map1PersonneAction($id){

        $modele=new Evenement();
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:Evenement')->find($id);
        return $this->render('SocialProMainBundle:Evenement:placeDetailsPersonne.html.twig',array('modele'=>$modele));
    }

    public  function postulerEvenementAction($id){

        $username=$this->get('security.token_storage')->getToken()->getUser();
        $userManager = $this->container->get('fos_user.user_manager');
        //$username="blabla";




        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Evenement')->find($id);

        //
        $postEvenement = new PostEvent();
        $modele->getId();
        // $id_emploi=$modele->getId();
        // $postEmploi->setIdEmploi($id_emp);
        $postEvenement->setIdEvent($modele);

        $id_user=$modele->getUser();
        $postEvenement->setUser($id_user);

        $postEvenement->setStatus("En cours");
        $postEvenement->setNombrePlace(1);
        $postEvenement->setTel(null);
        $postEvenement->setEmail($username);
        $postEvenement->setDate(new \DateTime('now'));
        $em->persist($postEvenement);
        $em->flush();




        return $this->redirectToRoute('afficher_Evenement');
    }
    public  function postulerEvenementPersonneAction($id){

        $username=$this->get('security.token_storage')->getToken()->getUser();
        $userManager = $this->container->get('fos_user.user_manager');
        //$username="blabla";




        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Evenement')->find($id);

        //
        $postEvenement = new PostEvent();
        $modele->getId();
        // $id_emploi=$modele->getId();
        // $postEmploi->setIdEmploi($id_emp);
        $postEvenement->setIdEvent($modele);

        $id_user=$modele->getUser();
        $postEvenement->setUser($id_user);

        $postEvenement->setStatus("En cours");
        $postEvenement->setNombrePlace(1);
        $postEvenement->setTel(null);
        $postEvenement->setEmail($username);
        $postEvenement->setDate(new \DateTime('now'));
        $em->persist($postEvenement);
        $em->flush();




        return $this->redirectToRoute('afficher_Evenement_personne');
    }



    public function abonnerEvenementAction( Request $ma_requette,$id){
        $modele=new  Evenement();

        // $user = $this->get('security.context')->getToken()->getUser();

        $form = $this->createForm(EvenementType::class,$modele);
        $form->handleRequest($ma_requette);
        if($form->isValid()){
            $em = $this->getDoctrine()->getManager();
            // $model->setUser($user);
            $em->persist($modele);
            $em->flush();
            return $this->redirectToRoute('afficher_Evenement');
        }
        return $this->render('SocialProMainBundle:Evenement:ajoutEvenement.html.twig',array('form'=>$form->createView()));

        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:Evenement')->find($id);

        $em->remove($modele);
        $em->flush();

        return $this->redirectToRoute('afficher_Evenement');

    }


    public  function afficherEvenementAction(){
        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Evenement')->findAll();
        return $this->render('SocialProMainBundle:Evenement:afficherMesEvenement.html.twig',array('modele'=>$modele));
    }
    public  function afficherEvenementPersonneAction(){
        $em=$this->getDoctrine()->getEntityManager();
        $modele=$em->getRepository('SocialProMainBundle:Evenement')->findAll();
        return $this->render('SocialProMainBundle:Evenement:afficherMesEvenementPersonne.html.twig',array('modele'=>$modele));
    }
    public function  supprimerEvenementAction($id){
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:Evenement')->find($id);

        $em->remove($modele);
        $em->flush();

        return $this->redirectToRoute('afficher_Evenement_ent');
    }


    public  function  modifierEvenementAction(Request $requette,$id){
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:Evenement')->find($id);

        $form=$this->createForm(EvenementType::class,$modele);
        $form->handleRequest($requette);
        if($form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();

            return $this->redirectToRoute('afficher_Evenement_ent');
        }
        return $this->render('SocialProMainBundle:Evenement:modifierEvenement.html.twig',array('form'=>$form->createView()));
    }
    public  function rechercherEvenementAction(Request $requette){
        $modele=new Evenement();
        $em=$this->getDoctrine()->getEntityManager();
        $form=$this->createFormBuilder($modele)
            ->add('nom')
            ->add('chercher',SubmitType::class)
            ->getForm();

        $form->handleRequest($requette);
        if($form->isValid()){
            $modele=$em->getRepository('SocialProMainBundle:Evenement')->findBy(array('nom'=>$modele->getNom()));
        }

        return $this->render('SocialProMainBundle:Evenement:rechercherEvenement.html.twig',array('form'=>$form->createView(),'modele'=>$modele));
    }

    public function AfficherPostEventAction(){
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:PostEvent')->findAll();

        return $this->render('SocialProMainBundle:PostEvent:afficherPostEvent.html.twig',array('modele'=>$modele));


    }

    public function supprimerPostEventAction($id){
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:PostEvent')->find($id);

        $em->remove($modele);
        $em->flush();

        return $this->redirectToRoute('AfficherPostEvent');


    }
    public function  RefuserEvenementAction($id){
        $em=$this->getDoctrine()->getManager();
        $etat=$em->getRepository('SocialProMainBundle:PostEvent')->find($id);
        $modeles=$em->getRepository('SocialProMainBundle:PostEvent')->findAll();
        $etat->setStatus('Refuser');
        $em->persist($etat);

        $em->flush();

       return  $this->redirect($this->generateUrl('AfficherPostEvent'));

    }

    public function AccepterEvenementAction($id){
        $em=$this->getDoctrine()->getManager();
        $etat=$em->getRepository('SocialProMainBundle:PostEvent')->find($id);
        $modeles=$em->getRepository('SocialProMainBundle:PostEvent')->findAll();
        $etat->setStatus('Accepter');
        $em->persist($etat);

        $em->flush();

         return $this->redirect($this->generateUrl('AfficherPostEvent'));
    }
    public  function rechercherPostEvenementAction(Request $requette){
        $modele=new PostEvent();
        $em=$this->getDoctrine()->getEntityManager();
        $form=$this->createForm(rechercherPostEvent::class,$modele);

        $form->handleRequest($requette);
        if($form->isValid()){
            $modele=$em->getRepository('SocialProMainBundle:PostEvent')->findBy(array('email'=>$modele->getEmail()));
        }

        return $this->render('SocialProMainBundle:PostEvent:rechercherPostEvent.html.twig',array('form'=>$form->createView(),'modele'=>$modele));
    }


    public  function  modifierPostEventAction(Request $requette,$id){
        $em=$this->getDoctrine()->getManager();
        $modele=$em->getRepository('SocialProMainBundle:PostEvent')->find($id);

        $form=$this->createForm(PostEventType::class,$modele);
        $form->handleRequest($requette);
        if($form->isValid()){
            $em=$this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();

            return $this->redirectToRoute('rechercherMyPostEvent');
        }
        return $this->render('SocialProMainBundle:PostEvent:modifierPostEvent.html.twig',array('form'=>$form->createView()));
    }


    public  function ajouterEvenement2Action(Request $requette){

        $modele=new Evenement();
        if($requette->isMethod('post')){
            $a=$requette->get('nom');

            $modele->setLibelle($a);
            $modele->setNom($requette->get('nom'));
            $modele->setLieu($requette->get('lieu'));
            $modele->setPays($requette->get('pays'));
            $modele->setDescription($requette->get('description'));
            $modele->setTheme($requette->get('theme'));
            $modele->setLat($requette->get('lat'));
            $modele->setLng($requette->get('lng'));
            $modele->setDateDebut($requette->get('datedebut'));
            $modele->setDateFin($requette->get('datefin'));
            $modele->setDate(new \DateTime('now'));
            $em=$this->getDoctrine()->getManager();
            $em->persist($modele);
            $em->flush();
            return $this->redirectToRoute('afficher_Evenement');

        }
        return $this->render('SocialProMainBundle:Evenement:ajouterEvents.html.twig');
    }



}
