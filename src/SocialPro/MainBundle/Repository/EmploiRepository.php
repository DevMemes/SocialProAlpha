<?php
namespace  SocialPro\MainBundle\Repository;
use Doctrine\ORM\EntityRepository;
/**
 * Created by PhpStorm.
 * User: Espoir
 * Date: 09/02/2017
 * Time: 22:14
 */
class EmploiRepository extends EntityRepository
{

    public function findPostEventEnCoursQB()
    {
        $query = $this->createQueryBuilder('espoir');
        $query->where("espoir.EnPromotion=:Promotion")->setParameter('Promotion', 0);
        return $query->getQuery()->getResult();


    }

    public function findPostEventValider()
    {
        $query = $this->createQueryBuilder('espoir');
        $query->where("espoir.EnPromotion=:Promotion")->setParameter('Promotion', 1);
        return $query->getQuery()->getResult();
    }
}