<?php

namespace SocialPro\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CommProjectLike
 *
 * @ORM\Table(name="comm_project_like")
 * @ORM\Entity(repositoryClass="SocialPro\ProjectBundle\Repository\CommProjectLikeRepository")
 */
class CommProjectLike
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\ProjectBundle\Entity\CommProject")
     */
    private $commProject;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCommProject()
    {
        return $this->commProject;
    }

    /**
     * @param mixed $commProject
     */
    public function setCommProject($commProject)
    {
        $this->commProject = $commProject;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


}

