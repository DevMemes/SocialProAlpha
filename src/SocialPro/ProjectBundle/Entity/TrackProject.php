<?php

namespace SocialPro\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use SocialPro\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TrackProject
 *
 * @ORM\Table(name="track_project")
 * @ORM\Entity(repositoryClass="SocialPro\ProjectBundle\Repository\TrackProjectRepository")
 */
class TrackProject
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\ProjectBundle\Entity\Projet")
     */
    private $project;

    /**
     * @ORM\ManyToOne(targetEntity="SocialPro\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @ORM\Column(name="montant", type="float")
     * @Assert\Range(
     *      min = 1,
     *      max = 999999999,
     *      minMessage = "the minimum donation must be at least {{ limit }}€",
     *      maxMessage = "the maximum donation must be at most {{ limit }}€"
     * )
     */
    private $montant;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param mixed $project
     */
    public function setProject(Projet $project)
    {
        $this->project = $project;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param mixed $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }


}

