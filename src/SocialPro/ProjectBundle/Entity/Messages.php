<?php
/**
 * Created by PhpStorm.
 * User: marwen
 * Date: 14/05/2017
 * Time: 22:55
 */

namespace SocialPro\ProjectBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Projet
 *
 * @ORM\Table(name="messages")
 * @ORM\Entity(repositoryClass="SocialPro\ProjectBundle\Repository\ProjetRepository")
 */
class Messages
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="sender", type="string", length=100)
     */
    private $nom;

    /**
     * @ORM\Column(name="reciever", type="string", length=255)
     */
    private $description;


    /**
     * @ORM\Column(name="message", type="string", length=255)
     */
    private $cout;

    /**
     * @ORM\Column(name="date", type="date")
     */
    private $domaine;

    /**
     * @ORM\Column(name="nonlu", type="integer", nullable=true)
     */
    private $status;

}