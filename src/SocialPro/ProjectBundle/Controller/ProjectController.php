<?php

namespace SocialPro\ProjectBundle\Controller;

use CMEN\GoogleChartsBundle\GoogleCharts\Charts\BarChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\Histogram;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use SocialPro\ProjectBundle\Entity\Projet;
use SocialPro\ProjectBundle\Entity\TrackProject;
use SocialPro\ProjectBundle\Form\ProjetType;
use SocialPro\ProjectBundle\Form\TrackProjectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ProjectController extends Controller
{

    public function controleAction($cp)
    {
       /* $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');

        $products = $repository->findoneBy(array('nom'=>$cp));

        */

        $repository = $this->getDoctrine()->getManager()->getRepository('ProjectBundle:Projet');
        $query = $repository->createQueryBuilder('p')
            ->where('p.nom LIKE :nom')
            ->setParameter('nom', $cp.'%')
            ->getQuery();

        $products = $query->getResult();



        if($products){
            $i=0;
            foreach ($products as $p){

                $nom[0][$i] = $p->getNom();
                $nom[1][$i] = $p->getDescription();
                $nom[2][$i] = $p->getId();
                $i++;

            }

        }

        else{
            $nom=null;
            }
        $response = new JsonResponse();
        return $response->setData(array('nom'=>$nom));



        /*$result = $em=$this->getDoctrine()->getRepository('ProjectBundle:Projet')->createQueryBuilder('o')
            ->where('o.OrderEmail = :email')
            ->andWhere('o.Product LIKE :product')
            ->setParameter('email', 'some@mail.com')
            ->setParameter('product', 'My Products%')
            ->getQuery()
            ->getResult();
        */

      /*  $repository = $this->getDoctrine()->getManager()->getRepository('ProjectBundle:Projet');
        $query = $repository->createQueryBuilder('p')
            ->where('p.nom = :nom')
            ->setParameter('nom', $cp)
            ->getQuery();

        $products = $query->getResult();

        var_dump($products);
       */
        //var_dump($products);


       /* $response= new JsonResponse();

        //$x = new JsonResponse();
        $x = $response->setData($products);
        var_dump($x);
        return $x;

       */
    }


    public function indexAction(Request $request)
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');

        $listProjects = $repository->findAll();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $listProjects,
            $request->query->getInt('p',1),
            $request->query->getInt('limit',3)
        );


        //dump(get_class($paginator));


        return $this->render('ProjectBundle:Project:index.html.twig',array(
            'project' => $result,
        ));
    }

    public function listAction(Request $request)
    {

        $session = $this->getUser();
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');

        $listProjects = $repository->findByUser($session);

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $listProjects,
            $request->query->getInt('p',1),
            $request->query->getInt('limit',3)
        );


        //dump(get_class($paginator));


        return $this->render('ProjectBundle:Project:liste.html.twig',array(
            'project' => $result,
        ));
    }

    public function addAction(Request $request)
    {
        $project = new Projet();
        $form = $this->createForm(ProjetType::class,$project);
        if ($form->handleRequest($request)->isValid()) {

        if ($form->isSubmitted()) {

            $recaptcha = new \ReCaptcha\ReCaptcha("6LcwvxQUAAAAAG23hLKAGr5SwsKPycNKGqftLDSa");
            $resp = $recaptcha->verify($_POST['g-recaptcha-response']);
            if ($resp->isSuccess()) {
                $session = $this->getUser();
                $project->setUser($session);
                $project->setStatus(1);
                $em = $this->getDoctrine()->getManager();
                $em->persist($project);
                $em->flush();
                return $this->redirectToRoute("project_myproject");
            } else {
                $errors = $resp->getErrorCodes();

            }

            $formView = $form->createView();
            return $this->render('@Project/Project/ajout.html.twig',array('form' => $formView));

        }

        }

        $formView = $form->createView();

        return $this->render('@Project/Project/ajout.html.twig',array('form' => $formView));
    }


    public function foundsAction(Request $request,$id)
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');

        $project = $repository->find($id);

        $trackProject = new TrackProject();
        $form = $this->createForm(TrackProjectType::class,$trackProject);
        if($form->handleRequest($request)->isValid()) {

            if ($form->isSubmitted()) {

                $repositoryTrack = $this
                    ->getDoctrine()
                    ->getManager()
                    ->getRepository('ProjectBundle:TrackProject');

                    $session = $this->getUser();
                    $trackProject->setProject($project);
                    $trackProject->setUser($session);
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($trackProject);
                    $em->flush();
                    //var_dump($em);
                $criteria = array("project" => $id);
                $trackProject = $repositoryTrack->findBy($criteria);
                //var_dump($trackProject);
                $donnation=0;
                foreach ($trackProject as $track) {
                    $donnation+=$track->getMontant();
                }

                return $this->render('ProjectBundle:Project:choix.html.twig',array(
                    'project' => $project,'montant' => $donnation
                ));


            }

        }

        $formView = $form->createView();




        return $this->render('@Project/Project/founds.html.twig',array('form' => $formView,
            'project' => $project));
    }

    public function searchAction($id)
    {
        return $this->render();
    }


    public function selectAction($id)
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');

        $project = $repository->find($id);

            $repositoryTrack = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:TrackProject');
        //$ex=$project->getId();
        $criteria = array("project" => $project,);
        $trackProject = $repositoryTrack->findBy($criteria);
        //$trackProject = $repositoryTrack->findBy(array('project_id' => $project->getId()));
        $donnation=0;
        foreach ($trackProject as $track) {
            $donnation+=$track->getMontant();
        }

        return $this->render('ProjectBundle:Project:choix.html.twig',array(
            'project' => $project,'montant' => $donnation
        ));
    }

    public function editAction(Request $request, Projet $project)
    {
        if ($this->getUser()->getId()==$project->getUser()->getId()){
        $form = $this->createForm(ProjetType::class,$project);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //$session = $this->getUser()->getId();
            //$project->setIdInd($session);
            $em = $this->getDoctrine()->getManager();

            //$em->persist($project);
            $em->flush();
            return $this->redirectToRoute("project_myproject");

        }


        $formView = $form->createView();

        return $this->render('@Project/Project/modifier.html.twig',array('form' => $formView));
    }

        return $this->redirectToRoute("project_homepage");
    }

    public function statistiquesAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');

        $listProjects = $repository->findAll();
            $a=0;
            $b=0;
            $c=0;
            $d=0;
            $f=0;
        foreach ($listProjects as $project) {
            // $advert est une instance de Advert
            $cout=$project->getCout();

            if($cout>0&&$cout<20000){
                $a+=1;
            }
            else if($cout>=20000&&$cout<50000){
                $b+=1;
            }
            else if($cout>=50000&&$cout<200000){
                $c+=1;
            }
            else if($cout>=200000&&$cout<500000){
                $d+=1;
            }
            else if($cout>=500000){
                $f+=1;
            }

        }










       /* $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');


        return $this->render('ProjectBundle:Project:choix.html.twig',array(
            'project' => $project,
        ));*/

        $pieChart = new PieChart();
        $pieChart->getData()->setArrayToDataTable(
            [['Task', 'Hours per Day'],
                ['<20000€',     $a],
                ['20000-49000€',      $b],
                ['50000-199000€',  $c],
                ['200000-499000€', $d],
                ['>500000€',    $f]
            ]
        );
        $pieChart->getOptions()->setTitle('Classement des Projets par tranche de coût');
        $pieChart->getOptions()->setHeight(500);
        //$pieChart->getOptions()->setWidth(900);
        $pieChart->getOptions()->getTitleTextStyle()->setBold(true);
        $pieChart->getOptions()->getTitleTextStyle()->setColor('#009900');
        $pieChart->getOptions()->getTitleTextStyle()->setItalic(true);
        $pieChart->getOptions()->getTitleTextStyle()->setFontName('Arial');
        $pieChart->getOptions()->getTitleTextStyle()->setFontSize(20);


        return $this->render('ProjectBundle:Project:Statistiques.html.twig', array('piechart' => $pieChart));
    }

    public function statDomaineAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');

        $listProjects = $repository->findAll();
        $a = "";
        $b = "";
        $c = "";
        $d = "";
        $e="";
        $f = "";
        $g="";
        $h="";
        foreach ($listProjects as $project) {
            // $advert est une instance de Advert
            $cout = $project->getDomaine();

            if ($cout=="Informatique") {
                $a += 1;
            } else if ($cout=="Telecommunication") {
                $b += 1;
            } else if ($cout=="GeniCivil") {
                $c += 1;
            } else if ($cout=="Mathematique") {
                $d += 1;
            } else if ($cout=="Physique") {
                $e += 1;
            }else if ($cout=="Mecanique") {
                $f += 1;
            }else if ($cout=="Electronique") {
                $g += 1;
            }else if ($cout=="Autre") {
                $h += 1;
            }



        }

        $bar = new BarChart();
        $bar->getData()->setArrayToDataTable([
            ['Domaine', 'Nombre de projets'],
            ['Informatique', $a],
            ['Telecommunication', $b],
            ['GeniCivil', $c],
            ['Mathematique', $d],
            ['Physique', $e],
            ['Mecanique', $f],
            ['Electronique', $g],
            ['Autre', $h]
        ]);
        $bar->getOptions()->setTitle('Classement des projets par domaine');
        $bar->getOptions()->getHAxis()->setTitle('Classement des projets par domaine');
        $bar->getOptions()->getHAxis()->setMinValue(0);
        $bar->getOptions()->getVAxis()->setTitle('projects');
        //$bar->getOptions()->setWidth(900);
        $bar->getOptions()->setHeight(500);

        return $this->render('ProjectBundle:Project:StatDomaine.html.twig', array('piechart' => $bar));

    }

    public function adminProjectAction(Request $request)
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');

        $listProjects = $repository->findAll();

        /**
         * @var $paginator \Knp\Component\Pager\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
            $listProjects,
            $request->query->getInt('p',1),
            $request->query->getInt('limit',20)
        );
        //var_dump($result);
        return $this->render('ProjectBundle:Admin:AdminProject.html.twig',array('project'=>$result));

    }

    public function adminProjectSupprimerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('ProjectBundle:Projet')->findOneBy(array('id' => $id));

        if ($entity != null){
            $em->remove($entity);
            $em->flush();
        }

        return $this->redirectToRoute("project_admin");
    }

    public function adminProjectConsulterAction($id)
    {

        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:Projet');

        $project = $repository->find($id);

        $repositoryTrack = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('ProjectBundle:TrackProject');
        //$ex=$project->getId();
        $criteria = array("project" => $project,);
        $trackProject = $repositoryTrack->findBy($criteria);
        //$trackProject = $repositoryTrack->findBy(array('project_id' => $project->getId()));
        $donnation=0;
        foreach ($trackProject as $track) {
            $donnation+=$track->getMontant();
        }

        return $this->render('ProjectBundle:Admin:AdminConsultProject.html.twig',array(
            'project' => $project,'montant' => $donnation
        ));

    }

}