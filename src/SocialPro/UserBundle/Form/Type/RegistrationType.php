<?php
namespace SocialPro\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Util\LegacyFormHelper;


class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',TextType::class,array('attr' => array('class' => 'form-email form-element large center','placeholder' => 'Nom Personne / Nom Entreprise'),'label'=>false))
           /* ->add('role', HiddenType::class, array('' =>"ROLE_INDIV",
                '' =>"ROLE_ENT")
            )
            /*->add('nom', HiddenType::class, array(
                'data' => 'abcdef'))*//*
           ->add('roles',ChoiceType::class, array(

               'choices' => array(
                   'Compte personel' => 'ROLE_INDIV',
                   'Compte Entreprise' => 'ROLE_ENT',
                   'Compte Admin' => 'ROLE_ENT'
               ),
               'multiple' => true,
               'expanded' => false,
               'required' => true,
               'data'     => 'Compte personel'
           ))*/
           ->add('roles', CollectionType::class, array(
                    'entry_type'   => ChoiceType::class,
                    'entry_options'  => array(
                        'choices'  => array(
                            'Compte personel' => 'ROLE_INDIV',
                            'Compte Entreprise'     => 'ROLE_ENT',
                        ),
                        'label'=>'Type compte',
                    ),
                    'attr' => array('class' => 'form-aux'),

               ))
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('translation_domain' => 'FOSUserBundle','attr' => array('class' => 'form-email form-element large center','placeholder' => 'Email'),'label'=>false))
            ->add('username', null, array('translation_domain' => 'FOSUserBundle','attr' => array('class' => 'form-email form-element large center','placeholder' => 'Username'),'label'=>false))
            ->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
                'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => 'form.password','attr' => array('class' => 'form-email form-element large center','placeholder' => 'Mot de passe'),'label'=>false),
                'second_options' => array('label' => 'form.password_confirmation','attr' => array('class' => 'form-email form-element large center','placeholder' => 'Confirmer mot de passe'),'label'=>false),
                'invalid_message' => 'fos_user.password.mismatch'
            ));
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getName()
    {
        return 'app_user_registration';
    }
}
