<?php

namespace SocialPro\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\MessageBundle\Model\ParticipantInterface;
use FOS\UserBundle\Model\User as BaseUser;


/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */

class User extends BaseUser implements ParticipantInterface
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string",length=255)
     */
    protected $nom;
    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $sexe=null;

    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $natureentreprise=null;


    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $pays=null;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $telentreprise=null;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    protected $datedecreation=null;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    protected $datedenaissance=null;
    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $avatar=null;
    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $role=null;
    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $facebookID;
    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $googleID;

    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */

    protected $linkedinID;
    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $description=null;

    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $domaine=null;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $status=null;

    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    public $skills=null;



    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $addresse=null;

    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $addressemailperso=null;

    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
   protected $job=null;

    /**
     * @ORM\Column(type="string",length=255,nullable=true)
     */
    protected $addressjob=null;


    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $entreprise=null;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param mixed $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @return mixed
     */
    public function getDatedenaissance()
    {
        return $this->datedenaissance;
    }

    /**
     * @param mixed $datedenaissance
     */
    public function setDatedenaissance($datedenaissance)
    {
        $this->datedenaissance = $datedenaissance;
    }

    /**
     * @return mixed
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param mixed $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getAddresse()
    {
        return $this->addresse;
    }

    /**
     * @param mixed $addresse
     */
    public function setAddresse($addresse)
    {
        $this->addresse = $addresse;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * @param mixed $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }

    /**
     * @return mixed
     */
    public function getFacebookID()
    {
        return $this->facebookID;
    }

    /**
     * @param mixed $facebookID
     */
    public function setFacebookID($facebookID)
    {
        $this->facebookID = $facebookID;
    }

    /**
     * @return mixed
     */
    public function getGoogleID()
    {
        return $this->googleID;
    }

    /**
     * @param mixed $googleID
     */
    public function setGoogleID($googleID)
    {
        $this->googleID = $googleID;
    }

    /**
     * @return mixed
     */
    public function getLinkedinID()
    {
        return $this->linkedinID;
    }

    /**
     * @param mixed $linkedinID
     */
    public function setLinkedinID($linkedinID)
    {
        $this->linkedinID = $linkedinID;
    }

    /**
     * @return mixed
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @param mixed $entreprise
     */
    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return mixed
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getAddressemailperso()
    {
        return $this->addressemailperso;
    }

    /**
     * @param mixed $addressemailperso
     */
    public function setAddressemailperso($addressemailperso)
    {
        $this->addressemailperso = $addressemailperso;
    }

    /**
     * @return mixed
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param mixed $job
     */
    public function setJob($job)
    {
        $this->job = $job;
    }

    /**
     * @return mixed
     */
    public function getAddressjob()
    {
        return $this->addressjob;
    }

    /**
     * @param mixed $addressjob
     */
    public function setAddressjob($addressjob)
    {
        $this->addressjob = $addressjob;
    }

    /**
     * @return mixed
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param mixed $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return mixed
     */
    public function getNatureentreprise()
    {
        return $this->natureentreprise;
    }

    /**
     * @param mixed $natureentreprise
     */
    public function setNatureentreprise($natureentreprise)
    {
        $this->natureentreprise = $natureentreprise;
    }

    /**
     * @return mixed
     */
    public function getDatedecreation()
    {
        return $this->datedecreation;
    }

    /**
     * @param mixed $datedecreation
     */
    public function setDatedecreation($datedecreation)
    {
        $this->datedecreation = $datedecreation;
    }

    /**
     * @return mixed
     */
    public function getDomaine()
    {
        return $this->domaine;
    }

    /**
     * @param mixed $domaine
     */
    public function setDomaine($domaine)
    {
        $this->domaine = $domaine;
    }

    /**
     * @return mixed
     */
    public function getTelentreprise()
    {
        return $this->telentreprise;
    }

    /**
     * @param mixed $telentreprise
     */
    public function setTelentreprise($telentreprise)
    {
        $this->telentreprise = $telentreprise;
    }




}
