<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;


class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new AppBundle\AppBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new SocialPro\MainBundle\SocialProMainBundle(),
            new SocialPro\UserBundle\SocialProUserBundle(),
            new SocialPro\ProjectBundle\ProjectBundle(),
            new SocialPro\EntrepriseBundle\SocialProEntrepriseBundle(),
            new Nomaya\SocialBundle\NomayaSocialBundle(), /* by fayrouz */
            new Liip\ImagineBundle\LiipImagineBundle(), /* by fayrouz */
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Discutea\DForumBundle\DForumBundle(),
            new HWI\Bundle\OAuthBundle\HWIOAuthBundle(),/* by Salim */
            new FOS\RestBundle\FOSRestBundle(),/* by salim */
            new FOS\CommentBundle\FOSCommentBundle(),/* by salim */
            new JMS\SerializerBundle\JMSSerializerBundle($this),/* by salim */
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),/* by salim */
            new CMEN\GoogleChartsBundle\CMENGoogleChartsBundle(),
            new FOS\MessageBundle\FOSMessageBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
